module Codec.Tilesetter where

import Data.Aeson
import Data.Text (Text, pack)
import GHC.Generics (Generic)
import Data.Vector (Vector)
import Control.Exception
import Control.Monad (replicateM)
import Data.List (unfoldr)

newtype DecodeError = DecodeError Text
  deriving stock (Eq, Show)
  deriving anyclass (Exception)

decodeFile :: FilePath -> IO Project
decodeFile path =
  eitherDecodeFileStrict' path >>= \case
    Left err ->
      throwIO . DecodeError $ pack err
    Right ok ->
      pure ok

data Project = Project
  { version :: Text
  , tilewidth :: Int
  , tileheight :: Int
  , tileshape :: Int -- top-down / isometric
  , sources :: Vector Source
  , relations :: Vector Relation
  , tileRelations :: Vector TileRelation
  , tiles :: Vector Tile
  , set :: Vector Set
  -- , set_regions :: Vector SetRegions
  , sandbox_layers :: Vector SandboxLayer
  , sandbox :: Vector Sandbox
  , extra :: Extra
  }
  deriving stock (Generic, Eq, Show)
  deriving anyclass (FromJSON, ToJSON)

data Source
  = Image
      { url :: Text
      }
  deriving stock (Generic, Eq, Show)
  deriving anyclass (FromJSON, ToJSON)

data Relation
  = WangBorder
      { matrix :: (Int, Int, Int, Int)
      , isBorder :: Bool
      }
  | Wang
      { origin :: Int
      , data_ :: Vector WangData
      }
  | Proxy
      { isReverse :: Bool
      , weight :: Int
      }
  deriving stock (Generic, Eq, Show)

instance FromJSON Relation where
  parseJSON = withObject "Relation" \o ->
    o .: "type" >>= \case
      "wang_border" -> do
        matrix <- o .: "matrix"
        isBorder <- o .: "isBorder"
        pure WangBorder{..}
      "wang" -> do
        origin <- o .: "origin"
        data_ <- o .: "data"
        pure Wang{..}
      "proxy" -> do
        isReverse <- o .: "isReverse"
        weight <- o .: "weight"
        pure Proxy{..}
      huh ->
        fail $ "Unexpected relation type: " <> show @Text huh

instance ToJSON Relation where
  toJSON = \case
    WangBorder{..} -> object
      [ "type" .= String "wang"
      , "matrix" .= matrix
      , "isBorder" .= isBorder
      ]
    Wang{..} -> object
      [ "type" .= String "wang"
      , "origin" .= origin
      , "data" .= data_
      ]
    Proxy{..} -> object
      [ "type" .= String "proxy"
      , "isReverse" .= isReverse
      , "weight" .= weight
      ]

data WangData = WangData
  { srcID :: Int
  , r :: Int
  , fx :: Bool
  , fy :: Bool
  , cutoff :: Maybe Int
  , c :: Maybe Bool
  }
  deriving stock (Generic, Eq, Show)
  deriving anyclass (FromJSON, ToJSON)

data TileRelation = TileRelation
  { r :: Int
  , tile :: Int
  , other :: Int
  }
  deriving stock (Generic, Eq, Ord, Show)
  deriving anyclass (FromJSON, ToJSON)

data Tile
  = TileWang
  | TileBase
      { sourceID :: Int
      }
  deriving stock (Generic, Eq, Show)

instance FromJSON Tile where
  parseJSON = withObject "Tile" \o ->
    o .: "type" >>= \case
      "wang" ->
        pure TileWang
      "base" -> do
        sourceID <- o .: "sourceID"
        pure TileBase{..}
      huh ->
        fail $ "Unexpected Tile type: " <> show @Text huh

instance ToJSON Tile where
  toJSON = \case
    TileWang -> object
      [ "type" .= String "wang"
      ]
    TileBase{..} -> object
      [ "type" .= String "base"
      , "sourceID" .= sourceID
      ]

data Set = Set
  { pos :: XY
  , tileID :: Int
  }
  deriving stock (Generic, Eq, Show)
  deriving anyclass (FromJSON, ToJSON)

data XY = XY
  { x :: Int
  , y :: Int
  }
  deriving stock (Generic, Eq, Ord, Show)
  deriving anyclass (FromJSON, ToJSON)

data Region = Region
  deriving stock (Generic, Eq, Show)
  deriving anyclass (FromJSON, ToJSON)

data SandboxLayer = SandboxLayer
  { visible :: Bool
  , name :: Text
  }
  deriving stock (Generic, Eq, Show)
  deriving anyclass (FromJSON, ToJSON)

data Sandbox = Sandbox
  { pos :: XYZ
  }
  deriving stock (Generic, Eq, Show)
  deriving anyclass (FromJSON, ToJSON)

data XYZ = XYZ
  { x :: Int
  , y :: Int
  , z :: Text -- XXX: yeah, really
  }
  deriving stock (Generic, Eq, Show)
  deriving anyclass (FromJSON, ToJSON)

data Extra = Extra
  { set :: Camera
  , sandbox :: Camera
  }
  deriving stock (Generic, Eq, Show)
  deriving anyclass (FromJSON, ToJSON)

data Camera = Camera
  { camX :: Float
  , camY :: Float
  , camZoom :: Float
  }
  deriving stock (Generic, Eq, Show)
  deriving anyclass (FromJSON, ToJSON)

{- | 4 corners or 4 edges for @n@ base tiles.

Index interpretation depends on the actual sample offsets.
-}
quadWeights :: Int -> [Int]
quadWeights numRoots =
  [ numRoots ^ (numSamples - 1 - i)
  | i <- [numSamples - 1, numSamples - 2 .. 0 :: Int]
  ]
  where
    numSamples = 4

{- | 8 quad directions for @n@ base tiles.

Index interpretation depends on the actual sample offsets.
-}
blobWeights :: Int -> [Int]
blobWeights numRoots =
  [ numRoots ^ (numSamples - i)
  | i <- [numSamples - 1, numSamples - 2 .. 0 :: Int]
  ]
  where
    numSamples = 8

-- *

allIndices
  :: (Int -> [Int])
  -> [Int]
  -> [Int]
allIndices mkWeights roots =
  fmap (lutIndex . mkWeights $ length roots) $
    permute mkWeights roots

{- | Convert neighbor values to LUT index
-}
lutIndex
  :: [Int] -- ^ Sample weights
  -> [Int] -- ^ Samples
  -> Int -- ^ LUT index
lutIndex weights = sum . zipWith (*) weights

unpackIndex :: Int -> Int -> [Int]
unpackIndex numRoots ix = unfoldr f (numRoots, ix)
  where
    f = \case
      (0, _) ->
        Nothing
      (pos, remainder) ->
        let
          (next, current) = divMod remainder numRoots
        in
          Just (current, (pos - 1, next))

-- | Provisional upper bound on LUT size
highestIndexValue :: (Int -> [Int]) -> Int -> Int
highestIndexValue mkWeights numRoots =
  lutIndex (mkWeights numRoots) $ repeat (numRoots - 1)

{- | Generate all the possible fillings for the given weights
-}
{-# SPECIALIZE permute :: (Int -> [Int]) -> [Int] -> [[Int]] #-}
permute
  :: (Applicative roots, Foldable roots, Foldable weights)
  => (Int -> weights w)
  -> roots r
  -> roots [r]
permute mkWeights roots = replicateM numWeights roots
  where
    numWeights = length $ mkWeights numRoots
    numRoots = length roots

-- *

cr31corners :: Num a => [(a, a)]
cr31corners = [rt, rb, lb, lt]

lt, rt, rb, lb :: Num a => (a, a)
lt = (-1, -1)
rt = (1, -1)
rb = (1, 1)
lb = (-1, 1)

cr31edges :: Num a => [(a, a)]
cr31edges = [t, r, b, l]

t, r, b, l :: Num a => (a, a)
t = (0, -1)
r = (1, 0)
b = (0, 1)
l = (-1, 0)

cr31blob :: Num a => [(a, a)]
cr31blob = [t, rt, r, rb, b, lb, l, lt]
