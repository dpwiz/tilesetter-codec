{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}
module Test where

import Control.Applicative ((<|>))
import Control.Monad (guard, mzero, unless)
import Data.Bool (bool)
import Data.Function (on)
import Data.List qualified as List
import Data.Ord (comparing, Down (..))
import Data.Traversable (for)
import Data.Vector qualified as Vector
import Data.Vector.Algorithms.Merge qualified as Merge
import Shower (printer)
import Test.Tasty.HUnit ((@?=))
import Data.IntMap qualified as IntMap

import Codec.Tilesetter qualified as Tilesetter
import Data.Foldable (for_)

data TileInfo = TileInfo
  { tileID :: Int
  , position :: Tilesetter.XY
  , info :: Tilesetter.Tile
  } deriving (Eq, Show)

unit_wang_pipeline :: IO ()
unit_wang_pipeline = do
  tset <- Tilesetter.decodeFile "assets/hw.tset"
  let
    leftTop = Vector.minimum $ Vector.map (.pos) tset.set

    tiles = Vector.modify (Merge.sortBy $ comparing (.tileID)) do
      Tilesetter.Set{pos=Tilesetter.XY{..}, tileID} <- tset.set
      let
        position = Tilesetter.XY
          { x = x - leftTop.x
          , y = y - leftTop.y
          }

      info <- Vector.indexM tset.tiles tileID

      pure TileInfo{..}

  -- printer (leftTop, Vector.take 1 tiles)
  (Vector.minimumBy (comparing (.position)) tiles).position @?=
    Tilesetter.XY 0 0

  -- let Just lava = Vector.find (\ti -> ti.position == XY 23 1) tiles
  -- printer lava
  -- Vector.indexM tiles (lava.tileID) >>= \same ->
  --   same @?= lava

  -- let
  --   lavaTouching =
  --     Vector.filter (\tr -> tr.tile == lava.tileID) tset.tileRelations
  --   touchingLava =
  --     Vector.filter (\tr -> tr.other == lava.tileID) tset.tileRelations
  let
    matrices = Vector.modify (Merge.sortBy $ comparing fst) do
      Tilesetter.TileRelation{r, tile, other} <- tset.tileRelations
      thisTile <- Vector.indexM tiles tile
      otherTile <- Vector.indexM tiles other
      Vector.indexM tset.relations r >>= \case
        Tilesetter.WangBorder{matrix} ->
          pure (matrix, (thisTile, otherTile))
        Tilesetter.Wang{} ->
          mzero
        Tilesetter.Proxy{} ->
          mzero

    groups = do
      items@((key, _) : _gs) <- List.groupBy (\a b -> fst a == fst b) $ Vector.toList matrices
      pure (key, List.nub $ map snd items)

  roots <- fmap (Vector.fromList . List.sortOn (.position)) .
    traverse (Vector.indexM tiles) $
      List.nub do
        (a, b, c, d) <- map fst groups
        [a, b, c, d]
  let
    toRoot t = Vector.findIndex (\r -> r.tileID == t) roots

  let
    cornerTiles = List.nub $ List.sortBy (comparing fst) do
      (corners@(a, b, c, d), candidates) <- groups
      (x, y) <- candidates
      candidate <- [x, y]
      guard (candidate.tileID `notElem` [a, b, c, d])
      pure (corners, candidate)

    corner2tile = Vector.fromList do
      items <- List.groupBy ((==) `on` fst) cornerTiles
      case items of
        [((a, b, c, d), TileInfo{tileID})] -> do
          let samples = map toRoot [a, b, c, d]
          case sequenceA samples of
            Nothing ->
              error "boo, corner is not a root"
            Just rootSamples ->
              pure (rootSamples, tileID)
        huh ->
          error $ "Too many items! " <> show huh

  -- printer $ take 3 corner2tile
  -- printer (roots, Vector.length corner2tile + Vector.length roots)

  Tilesetter.quadWeights 3 @?= [1, 3, 9, 27]

  let
    -- rootIds = fmap (.tileID) roots
    numRoots = Vector.length roots
    rootIndices = [0..numRoots - 1]
    weights = Tilesetter.quadWeights numRoots
  -- printer weights

  let
    proxies = fmap (List.sortOn Down) $ IntMap.fromListWith mappend $ Vector.toList do
      Tilesetter.TileRelation{r, tile, other} <- tset.tileRelations
      -- thisTile <- Vector.indexM tiles tile
      -- otherTile <- Vector.indexM tiles other
      Vector.indexM tset.relations r >>= \case
        Tilesetter.WangBorder{} ->
          mzero
        Tilesetter.Wang{} ->
          mzero
        Tilesetter.Proxy{isReverse, weight} -> do
          guard isReverse -- XXX: the weights are tied to the reverse relations
          case Vector.findIndex (\t -> t.tileID == tile) roots of
            Nothing ->
              -- XXX: not a root, skip for now
              mzero
            Just rootIx ->
              pure
                ( rootIx
                , [(weight, other)] -- XXX: alternative tile
                )

  let
    proxyVecs = do
      rootIx <- [0 .. numRoots - 1]
      root <- Vector.indexM tiles rootIx
      case IntMap.lookup rootIx proxies of
        Nothing ->
          pure [vec4sfloat @Float 0 0 0 (-1)]
        Just alternatives ->
          pure do
            (weight, tileID) <- alternatives
            tile <- Vector.indexM tiles tileID
            let
              item =
                if tile.tileID == root.tileID then
                  vec4sfloat
                    tile.position.x
                    tile.position.y
                    (4 - 1) -- XXX: proxies are bit more rough
                    0
                else
                  vec4sfloat
                    tile.position.x
                    tile.position.y
                    4 -- TODO: keep root settings somewhere
                    0

            pure $ mconcat
              [ "Vector.replicate "
              , show weight
              , " ("
              , item
              , ")"
              ]

  putStrLn $ unlines do
    item : rest <- proxyVecs
    pure $ concatMap unlines
      [ ["[ " <> item]
      , map (mappend ", ") rest
      , ["]"]
      ]

  fail "Proxy"

  let
    allKeys = Vector.fromList $ Tilesetter.permute Tilesetter.quadWeights rootIndices
  -- printer do
  --   key <- allKeys
  --   pure (key, Tilesetter.lutIndex weights key)

  let
    preLUT = Vector.modify (Merge.sortBy $ comparing fst) do
      key <- allKeys
      let
        isRoot =
          case List.nub key of
            [rootId] ->
              case Vector.indexM roots rootId of
                Nothing ->
                  error "root not preLUT, bruh"
                Just TileInfo{tileID} ->
                  Just tileID
            _ ->
              Nothing
      pure
        ( Tilesetter.lutIndex weights key
        , ( key
          , fmap snd (Vector.find ((==) key . fst) corner2tile) <|> isRoot
          )
        )

  (lastLutID, _) <- Vector.lastM preLUT
  unless (lastLutID + 1 == Vector.length preLUT) $
    fail $
      "Boo, LUT size doesn't match last element index: " <>
      show (lastLutID + 1, Vector.length preLUT)

  let
    tileLUT = do
      (_lutID, (_key, tileID)) <- preLUT
      for tileID (Vector.indexM tiles)

    positionLUT = Vector.map (maybe (Tilesetter.XY 255 255) (.position)) tileLUT

  -- printer corner2tile
  -- printer preLUT
  -- printer tileLUT
  -- printer positionLUT

  let
    vec4s = do
      (ix, mtile) <- Vector.imap (,) tileLUT
      let mess = Tilesetter.unpackIndex numRoots ix
      let
        (x, y) = case mtile of
          Nothing ->
            (255, 255)
          Just tile ->
            (tile.position.x, tile.position.y)
        z =
          if null mess || 0 `elem` mess then
            -- too hot to touch
            0
          else
            -- penalty for extra rough terrain, progressively harder for more contacting roots
            4 - length (List.nub mess)
        w = 0 :: Int -- TODO: flippin' flags

      pure $ mconcat
        [ "  , "
        , vec4sfloat x y z w
        , " -- "
        , show mess
        , " | "
        , show ix
        ]

  putStrLn "\n  "
  Vector.forM_ vec4s putStrLn

  -- TODO: fallback tile
  -- TODO: external tile props

vec4sfloat
  :: (Ord a, Show a, Num a)
  => a -> a -> a -> a
  -> String
vec4sfloat x y z w = mconcat
  [ "vec4 "
  , bool "" "(" $ x > 0
  , show x
  , bool "" "/255)" $ x > 0
  , " "
  , bool "" "(" $ y > 0
  , show y
  , bool "" "/255)" $ y > 0
  , " "
  , bool "" "(" $ z > 0
  , show z
  , bool "" "/255)" $ z > 0
  , " "
  , bool "" "(" $ w > 0
  , show w
  , bool "" "/255)" $ w > 0
  ]
